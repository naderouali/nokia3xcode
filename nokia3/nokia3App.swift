//
//  nokia3App.swift
//  nokia3
//
//  Created by Fares Othmane on 23/04/2021.
//

import SwiftUI

@main
struct nokia3App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
